module.exports = function (grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json')

    ,banner: '/*!\n <%= pkg.title || pkg.name %> - v<%= pkg.version %> - '
      + '<%= grunt.template.today("yyyy-mm-dd") %>\n'
      + ' Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;\n*/\n'

    ,concat: {
      options:{
        banner: '<%= banner %> ;(function( window ){ \n '
        ,footer: '}( window ))'
      }
      ,'dist': {
        src: ['src/*.js']
        ,dest: 'dist/<%= pkg.name %>.js'
      }
    }

    ,watch: {
      src: {
        files: '<%= concat.dist.src %>'
        ,tasks: 'concat'
        ,spawn: false
      }
    }

    ,mocha: {
      options: {
        bail: true
        ,log: true
        ,mocha: {
          ignoreLeaks: false
        }
        ,run: true
      }
      ,boardLogic: {
        src: 'test/boardLogic.html'
      }
    }

  })

  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-mocha')
  grunt.registerTask('default', ['mocha', 'concat'])

}