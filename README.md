###About
You must do next instructions in the same directory as Grunfile.js and package.json are.

* Install project dependencies. Type `npm install` in console. You might need `sudo` here.
* Build project. Type `grunt` in console.
* After that you can find game at `example/game.html`