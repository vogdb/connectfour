function InfoUI(gameDOM) {
  this._ui = $('<div/>').appendTo(gameDOM)
  this._currentPlayer = this._createField('Current player is:')
  this._winner = this._createField('Winner is: ')
}

InfoUI.prototype._createField = function (text) {
  var field = $('<div/>').appendTo(this._ui)
  field.append('<span>' + text + '</span><span class="color"/>')
  return field
}

InfoUI.prototype.setCurrentPlayer = function (player) {
  this._setField(this._currentPlayer, player.color, player.color)
}

InfoUI.prototype.setWinner = function (player) {
  this._setField(this._winner, player.color, player.color)
}

InfoUI.prototype._setField = function (field, text, color) {
  field.css('color', color)
  field.find('.color').text(text)
}

InfoUI.prototype.reset = function () {
  this._setField(this._winner, '', '#000')
}