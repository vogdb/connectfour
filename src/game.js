function game(options) {
  options = $.extend({}, BOARD, options)
  var boardLogic = new BoardLogic(options)
  var boardUI = new BoardUI(options)
  var controller = new TurnController()
  controller.addPlayer(new Player(COLORS.DEFAULT1))
  controller.addPlayer(new Player(COLORS.DEFAULT2))
  var infoUI = new InfoUI(options.canvas)
  infoUI.setCurrentPlayer(controller.currentPlayer())

  boardUI.on(BoardUI.EVENTS.CLICK_COLUMN, function (col) {
    try {
      var player = controller.currentPlayer()
      var coords = boardLogic.drop(col, player)
      boardUI.drawToken(coords, player.color)
      controller.nextPlayer()
      infoUI.setCurrentPlayer(controller.currentPlayer())
    } catch (err) {
      alert('Error: ' + err)
    }
  })

  boardLogic.on(BoardLogic.EVENTS.WINNER, function (evt) {
    infoUI.setWinner(evt.player)
    alert('Winner is: ' + evt.player)
    infoUI.reset()
    boardLogic.reset()
    boardUI.reset()
  })
}

this['game'] = game