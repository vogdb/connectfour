function BoardUI(options) {
  smokesignals.convert(this)
  this._buildUI(options)
  this._initEventListeners()
}

BoardUI.prototype._buildUI = function (options) {
  this._canvas = $('<table border="1"/>').appendTo(options.canvas)
  this._canvas.width(options.width * options.cellSize)
  this._canvas.height(options.height * options.cellSize)
  this._matrix = []
  for (var row = options.height; row >= 1; row--) {
    var rowUI = $('<tr/>').appendTo(this._canvas)
    this._matrix[row] = []
    for (var col = 1; col <= options.width; col++) {
      var cell = $('<td/>').appendTo(rowUI)
      this._matrix[row][col] = cell
    }
  }
}

BoardUI.EVENTS = {
  CLICK_COLUMN: 'click_column'
}

BoardUI.prototype._initEventListeners = function () {
  this._canvas.click(function (evt) {
    if (evt.target.nodeName === 'TD') {
      //column index starts from 1 but cellIndex from 0
      this.emit(BoardUI.EVENTS.CLICK_COLUMN, evt.target.cellIndex + 1)
    }
  }.bind(this))

}

BoardUI.prototype.drawToken = function (coords, color) {
  this._matrix[coords.row][coords.col].css('background-color', color)
}

BoardUI.prototype.reset = function () {
  for (var row = this._matrix.length - 1; row >= 1; row--) {
    for (var col = 1; col <= this._matrix[row].length - 1; col++) {
      this.drawToken({row: row, col: col}, 'white')
    }
  }
}