function BoardLogic(options) {
  //number of columns in this._matrix
  this._width = options.width
  //number of rows in this._matrix
  this._height = options.height
  this._matrix = []
  for (var row = 1; row <= this._height; row++) {
    this._matrix[row] = []
    for (var col = 1; col <= this._width; col++) {
      this._matrix[row][col] = null
    }
  }

  smokesignals.convert(this)
}

BoardLogic.prototype.reset = function () {
  for (var row = 1; row <= this._height; row++) {
    for (var col = 1; col <= this._width; col++) {
      this._matrix[row][col] = null
    }
  }
}

BoardLogic.EVENTS = {
  WINNER: 'winner'
}

BoardLogic.prototype.drop = function (col, player) {
  if (col < 1 || col > this._width) {
    throw new Error('Illegal column')
  }
  if (this._matrix[this._height][col]) {
    throw new Error(col + ' column is occupied')
  }
  var row
  for (row = 1; row <= this._height; row++) {
    if (!this._matrix[row][col]) {
      this._matrix[row][col] = player
      break
    }
  }
  setTimeout(this._checkWinner.bind(this, row, col), 0)
  return {col: col, row: row}
}

BoardLogic.WIN_LINE_SIZE = 4
BoardLogic.prototype._checkWinner = function (row, col) {
  var hasWinner = this._hasHorizontal(row, col)
    || this._hasVertical(row, col)
    || this._hasDiagonal(row, col)
  if (hasWinner) {
    this.emit(
      BoardLogic.EVENTS.WINNER,
      {coords: {col: col, row: row}, player: this._matrix[row][col]}
    )
  }
}

BoardLogic.prototype._hasHorizontal = function (row, col) {
  var player = this._matrix[row][col]
  var start = this._getLineStart(col)
  var stop = this._getLineStop(col, this._width)
  for (var x = start, hit = 0; x <= stop; x++) {
    if (this._matrix[row][x] === player) {
      hit += 1
    } else {
      hit = 0
    }
    if (hit === BoardLogic.WIN_LINE_SIZE) {
      return true
    }
  }
}

BoardLogic.prototype._hasVertical = function (row, col) {
  var player = this._matrix[row][col]
  var start = this._getLineStart(row)
  var stop = this._getLineStop(row, this._height)
  for (var y = start, hit = 0; y <= stop; y++) {
    if (this._matrix[y][col] === player) {
      hit += 1
    } else {
      hit = 0
    }
    if (hit === BoardLogic.WIN_LINE_SIZE) {
      return true
    }
  }
  return false
}

BoardLogic.prototype._hasDiagonal = function (row, col) {
  var player = this._matrix[row][col]
  var startY = this._getLineStart(row)
  var stopY = this._getLineStop(row, this._height)
  var startX = this._getLineStart(col)
  var stopX = this._getLineStop(col, this._width)
  var x, y, hit

  var startBLTR = Math.max(startX, startY)
  var stopBLTR = Math.min(stopX, stopY)
  for (x = startBLTR, y = startBLTR, hit = 0; x <= stopBLTR && y <= stopBLTR; x++, y++) {
    if (this._matrix[x][y] === player) {
      hit += 1
    } else {
      hit = 0
    }
    if (hit === BoardLogic.WIN_LINE_SIZE) {
      return true
    }
  }

  for (x = stopBLTR, y = startBLTR, hit = 0; x >= startBLTR && y <= stopBLTR; x--, y++) {
    if (this._matrix[x][y] === player) {
      hit += 1
    } else {
      hit = 0
    }
    if (hit === BoardLogic.WIN_LINE_SIZE) {
      return true
    }
  }
  return false
}

BoardLogic.prototype._getLineStart = function (coord) {
  return Math.max(coord - (BoardLogic.WIN_LINE_SIZE - 1), 1)
}

BoardLogic.prototype._getLineStop = function (coord, maxBoundary) {
  return Math.min(coord + (BoardLogic.WIN_LINE_SIZE - 1), maxBoundary)
}
