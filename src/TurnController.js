function TurnController() {
  this._players = []
  this._currentPlayerIndex = 0
}
TurnController.prototype.addPlayer = function (player) {
  this._players.push(player)
}

TurnController.prototype.currentPlayer = function () {
  return this._players[this._currentPlayerIndex]
}

TurnController.prototype.nextPlayer = function () {
  this._currentPlayerIndex = (this._currentPlayerIndex + 1) % this._players.length;
}