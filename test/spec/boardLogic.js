describe('game board logic', function () {

  beforeEach(function () {
    this.board = new BoardLogic(BOARD)
  })

  it('find 4 vertical siblings of same color', function (done) {
    var board = this.board
    board.once(BoardLogic.EVENTS.WINNER, function(){done()})

    board.drop(2, COLORS.DEFAULT1)
    board.drop(2, COLORS.DEFAULT1)
    board.drop(2, COLORS.DEFAULT1)
    board.drop(2, COLORS.DEFAULT1)
  })

  it('find 4 horizontal siblings of same color', function (done) {
    var board = this.board
    board.once(BoardLogic.EVENTS.WINNER, function(){done()})

    board.drop(1, COLORS.DEFAULT1)
    board.drop(2, COLORS.DEFAULT1)
    board.drop(3, COLORS.DEFAULT1)
    board.drop(4, COLORS.DEFAULT1)
  })

  it('find 4 diagonal(bottom-left to top-right) siblings of same color', function (done) {
    var board = this.board
    board.once(BoardLogic.EVENTS.WINNER, function(){done()})

    board.drop(1, COLORS.DEFAULT1)
    board.drop(2, COLORS.DEFAULT2)
    board.drop(3, COLORS.DEFAULT2)
    board.drop(4, COLORS.DEFAULT2)

    board.drop(2, COLORS.DEFAULT1)
    board.drop(3, COLORS.DEFAULT2)
    board.drop(4, COLORS.DEFAULT2)

    board.drop(3, COLORS.DEFAULT1)
    board.drop(4, COLORS.DEFAULT2)

    board.drop(4, COLORS.DEFAULT1)
  })

  it('find 4 diagonal(bottom-right to top-left) siblings of same color', function (done) {
    var board = this.board
    board.once(BoardLogic.EVENTS.WINNER, function(){done()})

    board.drop(1, COLORS.DEFAULT2)
    board.drop(2, COLORS.DEFAULT2)
    board.drop(3, COLORS.DEFAULT2)
    board.drop(4, COLORS.DEFAULT1)

    board.drop(1, COLORS.DEFAULT2)
    board.drop(2, COLORS.DEFAULT2)
    board.drop(3, COLORS.DEFAULT1)

    board.drop(1, COLORS.DEFAULT2)
    board.drop(2, COLORS.DEFAULT1)

    board.drop(1, COLORS.DEFAULT1)
  })

})